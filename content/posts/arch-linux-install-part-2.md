---
title: "How to Install a Encrypted Arch Linux"
date: 2019-06-11
draft: true
---

- login as root (no password) and change its password:
$ passwd

- create new user that can use sudo

- install the graphical interface
	- gnome, gdm, NetworkManager

- enable gdm service and NetworkManager service

- setup locale

- install basic packges
	- git, fonts, firefox, gvim, vlc, thunderbird, etc

- reboot and login to your desktop environment with your user :)



## First things first: what is Arch Linux?

<b>Arch Linux</b> is a lightweight GNU/Linux distribution that tries to <b>Keep It
Simple</b>, as they say in their website. Some people say that is not a good
beginner's distro, since the install process is not about pressing "next"
buttons.

I <b>agree</b> with that. But don't think that it's only for advanced hackers
from other planets. It's just that it's not a good distro for those who aren't
trying to learn about computer science or don't have the time (or will) to
install and configure it. And <b>that's totally fine!</b>

It's true that this is a harder distribution to install (if you're a beginner),
but you'll be rewarded in the end. Arch Linux is really lightweight and things
are going to work the way <b>you</b> want! Also, you'll learn a lot during the
install process and usage.

<center>
<img src="/posts/arch-linux-install-part-1/arch-logo.png"
     alt="Arch Linux logo"
     style="width:500px; margin:10px 0px 5px 0px" />
<p> Arch Linux logo. A powerful lighweight GNU/Linux distribution. </p>
</center>

## My intention here...

It's not my intention to substitute the Arch Linux Wiki, which is very
rich, helpful and with a lot of skilled people contributing to it. It's the
only officially supported way to install this distribution, and I use it even
to fix problems in other distributions. I'm writing this guide to clarify
some stuff that I though were hard to comprehend at my first time installing
it. Feel free to contact me if you find any mistake or have some suggestions!

Enough talk. Let's do it!

## What do I need to install it?

#### Your machine:
- x86_64 compatible machine (32-bit processors are not supported; look for
<b>Arch Linux 32</b> if that's your case)
- 512MB RAM (recommended at least 2GB RAM)
- 800MB disk space (recommended at least 20GB disk)

#### Other stuff:
- A pen drive with a minimum of 2GB storage capacity
- Wired internet connection (you can use tethering if you don't have
wired internet connection available)
- I'm assuming that you're not going to do a dual-boot, since it's your
first time installing Arch Linux
- Power of will and a whole afternoon
- Paper and pen (not kidding)
- Silvertape (just kidding)

## Creating a bootable Arch Linux pendrive

The first step is to create a bootable Arch Linux pen drive. This is easier
if you are using a <b>GNU/Linux distribution</b>. Download Arch from the
official website, plug your pendrive to the USB port, and then run the
following command (you'll probably have to run it using <b>sudo</b>):

```
dd bs=4M if=path/to/archlinux.iso of=/dev/sdx status=progress oflag=sync
```

> Note that you have to replace <b>path/to/archlinux.iso</b> with the path of
the file you've just downloaded and <b>sdx</b> with the name of the USB drive
(find this using the command <b>lsblk</b>)

In case you're running <b>Windows</b>, I recommend you to use Rufus. It's
a pretty easy software to use, but remember to use this option:
<b>Partition scheme: GPT</b> and then select `dd`. MBR partition scheme
only works with Legacy BIOS, and we're not using it. GPT partition scheme
is the way to go with UEFI. In the next section we're going to give some details
about this.

## UEFI firmware vs Legacy BIOS firmware

Both <b>UEFI</b> and <b>Legacy BIOS</b> are softwares that comes pre-installed in your
computer's mother board and are used before the boot process. It's the <b>first
software</b> to run when your computer is powered on, being responsible for
powering the hardware, verifying if everything is working well and then
running the <b>bootloader</b> (the program that is responsible for loading your
operational system). <b>UEFI is a modern solution</b>, being considered faster,
more secure and supporting bigger disks. If you're not using a really old
computer, probably it has UEFI and you should use it. So, do the following
steps:

- Access your BIOS setup. Unfortunately, each computer has a unique way to do
this, so you'll have to find out yourself.
- Select UEFI in the Boot Mode.
- Change the Boot Priority to boot USB first.
- Disable Secure Boot. No, I'm not kidding. This is just temporary, so we
can boot the Arch Linux pen drive and your computer doesn't recognize it as a
malware, since it's not signed. I won't give details about this, but you can
search in the Internet if you don't trust me (and you're absolutely right
to don't).

> I'll assume from here on that you have UEFI enabled and Secure Boot Disabled in your computer.

## Booting Arch Linux live environment

Now, just plug your pen drive and power on your computer. You'll see something
like this:

> Notice that this screenshot is old, since Arch Linux does not support i686
(32-bit processors) anymore. Select the **first option: Boot Arch Linux (x86_64)**.

<center>
<img src="/posts/arch-linux-install-part-1/arch-boot.png"
     alt="Arch Linux live environment boot screen"
     style="width:500px; margin:10px 0px 5px 0px" />
<p> Arch Linux live environment boot screen. </p>
</center>

## Partitioning your disk

If everything is working well, you'll receive a black terminal as a
gift :confused:.<br/> Right now you may be pissed, but you have no idea
how powerful this is. You have full control of how the installation
process is going to work.

Now, let's start partitioning your disk, so we can split the boot and
the operational system into two different disk regions. Also, we can choose
the filesystem we're going to use for each disk partition. Fortunately,
Arch Linux live environment comes with certain programs to do this. We
are going to use `cfdisk`, which is very good. Run `lsblk`, find out
the name of your disk (in my case it's `/dev/sda`). Then, run the
following code:

```
sudo cfdisk /dev/sdx
```

If everything is fine, you'll see something like this:

```
Disk: /dev/sdx
Size: A GiB, B bytes, C sectors
Label: gpt, identifier: D
```

Pay attention to the field **Label: gpt**. It has to be exactly like this.
If it isn't, you have to change your disk partition scheme from MBR to GPT.
To do that, run `gfdisk /dev/sdx`. It'll automatically convert from MBR to
GPT. After that, you can run `sudo cfdisk /dev/sdx` and check the label again.

Now, we have a screen like this:

<center>
<img src="/posts/arch-linux-install-part-1/cfdisk.png"
     alt="Cfdisk main screen"
     style="width:500px; margin:10px 0px 5px 0px" />
<p> Cfdisk. A powerful tool to manage your disk partitions. </p>
</center>

Delete everything and let's create the partitions for your system. After all,
we're going to have:

1. Boot partition. Where your bootloader is going to be installed, a software
that is responsible for loading your operational system.
2. LVM (Logical Volume Manager). Where your operational system is going to
be installed. A LVM can be viewed as a virtual partition. It's more powerful
and flexible. For instance, it allows you to create a partition with multiple
disks and to expand the max amount or partitions that your disk can manage.

> For the **boot partition**, we're going to need **512MB**. The rest is going
to be allocated for the **LVM**.

To do this, select **New**, type `512M` and press enter. After that, move to
**Free Space** again, select **New** and press enter, if you want
to use the whole free space for your Arch Linux, or change the size and
then press enter. I recommend at least `50G` for your system.

Now, move to the **boot partition (512MB)** and select **Type**. Choose
`Efi Partition` and press enter. Do the same for the **LVM**,
but choose the **Type** `Linux LVM`.

After all, select **Write** (if everything looks like the screenshot bellow).
And that's it! **You've just managed to partition your disk** :satisfied:.

<center>
<img src="/posts/arch-linux-install-part-1/cfdisk-end.png"
     alt="Cfdisk after partition"
     style="width:500px; margin:10px 0px 5px 0px" />
<p> Cfdisk after partition. </p>
</center>

## Encrypting your disk with LUKS

You may be asking, "Why am I going to need this? I can just use the option
to ask for password before login." That's true, and you should use this. But
that's not enough, since it doesn't encrypt your disk. There are **lots of
easy attacks** that can be made to gain access to your personal data.

Now that I convinced you (I hope so), let's encrypt your disk. It's only
necessary to encrypt your **LVM**, the **bootloader** remains instact. Type
the following code:

```
sudo cryptsetup -v luksFormat /dev/sdx2
```

> We're using the default LUKS options. It uses AES with 256-bit
key size, which is pretty good. Of course you can change this, if
you have enough cryptography knowledge.

> The command above is going to ask you a password. **DON'T LOSE THIS**.
There's **no way** to recover your data if you lose this. This is the worst
disadvantage of disk encryption, in my opinion.

And that's it! Now your **LVM partition is encrypted** :satisfied:.

## Partitioning your LVM

We've mentioned that LVM is not a real partition. That means that we still
need to create the partitions inside your LVM. First of all, let's open
your encrypted LVM to manage it:

```
sudo cryptsetup luksOpen /dev/sdx2 luks
```

> It's going to ask for your the password you used to encrypt.

Now, we need to create a **Physical Volume (PV)**. That just means that we're
going to write some **LVM metadata** to your disk so it can be a part of a
**Volume Group (VG)**:

```
sudo pvcreate /dev/mapper/luks
```

With the PV created, we can finally create a **Volume Group (VG)**. We could
add various disks to this group and then use them together, as one logical
disk. I gave the name "arch" to it, but you could use another one.

```
sudo vgcreate arch /dev/mapper/luks
```

Now we're going to create the "real partitions" inside the VG. They are
called **Logical Volume (LV)**. There are some people that may prefer to
separate **root** and **home** partitions. I don't like doing this (because
it's unecessary for my use case), but I'm going to cover it.

> Notice that I've used 8GB for **swap partition**. If you don't know yet,
swap is used for hibernation and also when your system uses your whole RAM
memory. I have 8GB RAM memory and I like to use 8GB for swap, but this is
not a rule. It depends on way you're
going to use your computer and also how much memory/disk do you have.

> Also, I've used 30GB for **root partition**, but you can change this
according to your preferences.

- with root/home together:

```
sudo lvcreate -L 8GB -n swap arch
sudo lvcreate -l 100%FREE -n root arch
```

- with root/home separated:

```
sudo lvcreate -L 8GB -n swap arch
sudo lvcreate -L 30GB -n root arch
sudo lvcreate -l 100%FREE -n home arch
```

Now, we need to set the correct filesystem for the LV's we've created. Root
and home partitions are going to use the Ext4 filesystem, and the boot
partition is going to use Fat32.

> **Ext4** is the first choice for common users. It's tried and trusted.

> **Fat32** is a good filesystem for bootloaders because it' going to work
with both Windows and GNU/Linux. In the future, you may want to do a **dual
boot** (although I hope you don't - just kidding :innocent:).

```
sudo mkfs.ext4 /dev/mapper/arch-root
sudo mkfs.ext4 /dev/mapper/arch-home
sudo mkfs.vfat -F32 /dev/sdx1
```

Now, we need to add some metadata to the swap partition (so it can work
properly):

```
sudo mkswap /dev/mapper/arch-swap
```

Now we're going to connect to the internet and finally going to install
Arch Linux!

## Connecting to the Internet

It's easier if you have an Internet cable available or a cellphone that is
capable of **tethering** (nowadays any of them are). Plug the Internet cable
(or the USB cable, in case of tethering). Then, type the following:

```
sudo ip link
```

You'll see the Internet interfaces your computer has available. In my case,
`enp2s0` is the one for the wired connection. Let's broadcast ask for an IP
for the router:

> You may see something that starts with `e` and something that starts with
`w`. Use the one that starts with `e`, which is the interface for the wired
connection.

```
sudo dhcpcd enp2s0
```

Now everything should be working. Try it:

```
sudo ping archlinux.org
```

And now you're connected to the internet!

## Installing Arch Linux

First, you need to mount the partitions you've just setup.
That's something the **bootloader** does when you turn on your computer.

```
sudo mount /dev/mapper/arch-root /mnt
sudo mkdir /mnt/boot
sudo mount /dev/sdx1 /mnt/boot
sudo swapon /dev/mapper/arch-swap
```

NOOOW we're going to **install Arch Linux**. Let's install the base system
in the root partition:

```
sudo pacstrap /mnt base base-devel
```

In your new Arch Linux, we need to create the **fstab** file. It's
resposible to **mount your partitions**, since the bootloader is only
going to mount the **root partition**:

```
sudo genfstab -pU /mnt >> /mnt/etc/fstab
```

After that we can leave the live environment and enter **your Arch Linux**.
Let's just get the **root UUID** (the ID of the Logical Volume where your
system is installed) first, so when we configure the bootloader we can tell
it where to load your system:

```
sudo blkid /dev/sdx2
```

> You need to **take note** or **take a photo**.

Finally, access your Arch Linux:

```
sudo arch-chroot /mnt
```

## Installing and configuring the bootloader

First of all, I must say I'm **not going to use GRUB** bootloader. I just
don't like it. There are some cases when it's necessary, it has many more
features (which are unnecessary for me). The one I like to use is
**systemd-boot**. Let's install it:

```
sudo bootctl –-path=/boot install
```

When you power on your computer and the booloader is loaded, it will look
for an **entry**. So, we need to create one for the recent installed
Arch Linux:

```
sudo nano /boot/loader/entries/arch.conf
```

Add the following lines to it:

```
title   Arch Linux
linux   /vmlinuz-linux
initrd  /initramfs-linux.img
options cryptdevice=UUID=TheUUIDYouTookNote:arch resume=/dev/mapper/arch-swap root=/dev/mapper/arch-root rw quiet
```

> Note: **use tabs** in this file instead of spaces. Also, don't forget
to **replace** `TheUUIDYouTookNote` with the UUID you took note.

Finally, let's put the entry you've just created as the default entry:

```
sudo nano /boot/loader/loader.conf
```

And add this lines to it:

```
timeout 3
default arch
editor 0
```

> You can **change the timeout to 0**, since we're not doing dual boot.
But I recommend you try first with 3 seconds, so you can see the bootloader
for once. After that you can edit this file again.

## Creating the initial ramdisk

We're almost done. The only thing left to do is to configure the **initial
ramdisk**, a scheme used to load a temporary system into memory that is
responsible for loading the GNU/Linux system. In resume, it's used to
prepare the mount of your system and it vary from user to user (depending
if you use or not LVM, for instance). So, edit this file:

```
sudo nano /etc/mkinitcpio.conf
```

Change the following line:

```
HOOKS=(base udev autodetect modconf block filesystems keyboard fsck)
```

Leave it like this:

```
HOOKS=(base udev autodetect modconf block keymap encrypt lvm2 filesystems resume keyboard fsck)
```

Now, run the following command. It'll create the **initial ramdisk**.

```
sudo mkinitcpio -p linux
```

## It's over!

Just leave the system, unmount everything and reboot!

```
sudo exit
sudo umount -R /mnt
sudo reboot
```

In the next post I'm going to teach you **how to configure** your
recently installed system. Bye-bye!
