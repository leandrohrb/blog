---
title: "GNU/Linux Graphics Stack"
date: 2019-11-08
draft: true
---

## Introduction

In the last post I've tried to introduce the history of the most important 3D
graphics APIs and how they evolved. Now I'll try to take a look with depth in
the GNU/Linux graphics stack. If you find something wrong or can help me to
enhance this blog, please contact me, I'll be thankful if you do so :)

### GNU/Linux Graphics Stack

Nowadays the GNU/Linux graphics stack is robust and flexible, presenting many
advantages for graphics developers and the applications they create. So let's
take a look how this evolved since the time when things were much simpler.

### X and Wayland

<small>
Note: I never really knew what was going on with X that its developers started
to write Wayland, so I thought: "a Wayland developer certainly know how to
explain what is going on". Then I've found this talk given by Daniel Stone,
well known for his contributions in the GNU/Linux graphics stack. He talks
about this topic in a simple and really funny way. The text below is my
interpretation from what he says, but I strongly recommend you to watch his
talk.
</small>

In the past, things were not that complex. Although the computer power was a
limiting factor, we had only a few GPUs and 2D applications. X was doing great,
but things started to get a little complicated. As time passed by, the
complexity of the hardware started to get higher, and X tried to add as many
features as possible, to be able to support everything that was coming up. Many
of these features just don't make any sense anymore, and the code started to
get more and more bloated as time passed by. Daniel Stone even mentions that X
was something like a bad written OS instead of a window manager, since it was
handling everything in a dumb way. It was the opposite of the famous Unix
philosophy.

X contributors removed a lot of code from it and also modularized it. But the
code was so bloated that it was not enough. Nowadays, many things are done in
client side, since no one recommends to change how X is working. So X is not
doing so much anymore: the client just send to X what should be drawn and X put
it on a certain window. But that's not the only problem: in order to achieve
this X also does many unnecessary stuff, making it slower in many situations.
Basically, X server couldn't evolve because of its outdated architecture and
something better was necessary, since there are many bugs that just can't
be solved and enhancements that cannot be done.

The graphics developers started to think about something that would be **ideal
for the future**. The **simple architecture** they've created works like this:

- client decides what to draw locally
- client tells server what they've drawn
- the server decides what do drawn and where

This was the born of **Wayland**, in which "every frame is perfect". This means
no flicker, no flashes and no tearing.





When we hear about graphics programming we tend to think about games and
graphical interfaces. They are good examples and probably helped a lot in the
development of this area, but there are many other important applications that
can make use of it: art, design and simulations, for instance.

Not long ago, the computers used to work **without any GUI** (icons, images,
symbols, and not only text). The first one arrived in 1973, in a machine named
Xerox Auto. At that time, the development of the graphics stack in many systems
was in full swing, but the demand was only for **2D applications**.

<center>
<img src="/posts/3d-apis/xerox-alto.jpg"
     alt="Xerox Alto and its GUI"
     style="width:300px; margin:20px 0px 15px 0px" />
<p> Xerox Alto and its GUI. </p>
</center>

### OpenGL and Direct3D

This reality started to change in the 90's, when the home computer power was
big enough that people started to work on **3D applications in mass scale**.
This revolution was followed by the born of the first 3D capable GPUs and also
the frameworks **OpenGL and Direct3D**, which allowed 3D rendering with
hardware acceleration. Nothing was consolidated, so there was different ideas
coming from many game developers and this **lack of orientation** slowed down
the OpenGL development and created an uncertainty feeling about the future of
the technology. Direct3D was doing great, adding new features and making the
life of a game developer simpler. The game industry was rising strong, and they
couldn't count with something that could die in a brief future. We can't ignore
the marketing power of Microsoft as well.

<center>
<img src="/posts/3d-apis/opengl-directx.png"
     alt="OpenGL and DirectX logos"
     style="width:450px; margin:20px 0px 15px 0px" />
<p> The competitors: OpenGL and DirectX </p>
</center>

**OpenGL always had the advantaged to make easier to write multi-platform
applications**, but as time passed by creating games for Windows started to get
easier using Direct3D. Besides that, game developers started to get specialized
in Direct3D. This **created a loop**, in which the GPU vendors
focused to give a better support for D3D and graphics programmers specialized
even more in the technology. Also, with a much smaller community around the
OpenGL drivers it was hard to find the bugs for it.

### Microsoft and the Windows Vista episode

After the Vista release, Microsoft was trying to force users to migrate from
XP, so they've enabled support for D3D10 **only for Vista** (which was bad),
while XP users could only use D3D9 (even if they had hardware support for
D3D10). Fortunately, OpenGL was the way to go in the GNU/Linux graphics stack.
**With time and a strong community around it, OpenGL aged really well**. With
the Vista episode people started to realize how bad can be the monopoly of a
whole industry and the importance of OpenGL for the **future of graphics
programming**.  At that time, OpenGL had already created new features and was
faster than D3D9.  Nowadays, D3D11 is really optimized as well, but OpenGL's
portability still make it a strong competitor against Direct3D.

### Brand-new 3D APIs: Vulkan and Direct3D 12

In 2015, Khronos Group announced the **Vulkan API**, a low-level, low-overhead
and cross-platform 3D API. It offers **high performance** and is **less CPU
hungry**.  Besides that, it's able to distribute balance between multiple CPU
cores. This is really important for mobile applications, since it means less
power consumption. Also, OpenGL survived many technology revolutions, and it
makes sense to think that it may be a little bottled. Notice that this does not
means that OpenGL is bad and will be deprecated soon, since it's good enough
for the majority of the use cases. Vulkan is harder to learn and the developer
have to explicit in everything he's doing (this is exactly what makes it so
powerful), what consumes more development time.

MS also created a low-level API named D3D12. It's supported both by Win 7 / 10
and the GPU of Xbox One. The familiarity of many companies with D3D and the
number of Win/Xbox users are one of the reasons why many companies are still
developing for D3D12 instead of Vulkan.

<center>
<img src="/posts/3d-apis/vulkan-directx12.jpg"
     alt="Vulkan and DirectX12 logos"
     style="width:500px; margin:20px 0px 15px 0px" />
<p> DirectX 12 and Vulkan </p>
</center>

### Wine and Proton

We may think that because a program is created only for Windows it won't run in
a GNU/Linux distribution, but that's not true. **Wine** is a compatibility
layer that allow applications developed for Windows to run on GNU/Linux and
other Unix-like systems (like macOS), but it's not easy to configure and use.
So there are other applications that were built using it: **Winetricks,
PlayOnLinux, Lutris, etc**. But one software that deservers attention is
**Proton**, a variation of Wine created by Valve and CodeWeavers in 2018.
This is a huge effort to make Steam able to offer a plug-and-play experience
for other systems, and not only Windows. The main goal of Valve is probably
to make SteamOS (Debian based) and the Steam Machine viable.

With this release, the majority of Windows games are **running pretty well** on
Linux. Sometimes the users have to do some minor tweaking to make it work, but
nothing really hard. This is really good, since one of the main reasons why
people don't migrate from Windows or are doing dual-boot is because of gaming.
Most of the games that are not working properly are those that uses anti-cheats
that **spy our processes** to find something suspicious. Honestly, I think we are
better without them...

Getting back to talk about the 3D APIs, Proton added a compatibility layer
between Direct3D and OpenGL/Vulkan in order to make many Windows games to work
on Linux. Those are called **translation layers**:

- **D9VK**: Direct3D 9 -> Vulkan
- **DXVK**: Direct3D 10 / 11 -> Vulkan (also works for OpenGL)
- **VKD3D**: Direct3D 12 -> Vulkan

<center>
<img src="/posts/3d-apis/protondb.png"
     alt="Vulkan and DirectX12 logos"
     style="width:500px; margin:20px 0px 15px 0px" />
<p> ProtonDB: a database where people add reports and small tweaks to make games work with Proton. </p>
</center>

### And that's all for today

I hope I've managed to clarify some stuff for you that is beginning to learn
graphics programming :) In the next blog-post I'll try to explain a little bit
about the GNU/Linux graphics stack.

### Further Reading

https://www.collabora.com/news-and-blog/blog/2018/12/17/a-dream-come-true-android-finally-using-drm/kms/
https://blogs.igalia.com/itoral/2014/07/29/a-brief-introduction-to-the-linux-graphics-stack/

https://en.wikipedia.org/wiki/Computer_graphics</br>
https://en.wikipedia.org/wiki/3D_computer_graphics</br>
https://en.wikipedia.org/wiki/History_of_the_graphical_user_interface</br>
https://en.wikipedia.org/wiki/OpenGL</br>
http://blog.wolfire.com/2010/01/Why-you-should-use-OpenGL-and-not-DirectX</br>
https://pt.wikipedia.org/wiki/Vulkan</br>
https://www.imgtec.com/blog/stuck-on-opengl-es-time-to-move-on-why-vulkan-is-the-future-of-graphics/</br>
https://www.reddit.com/r/opengl/comments/3pdtk1/what_are_the_strength_and_weaknesses_of_opengl/</br>
https://www.reddit.com/r/linux/comments/81114g/why_do_game_companies_still_use_directx_instead/</br>
https://en.wikipedia.org/wiki/Proton_(compatibility_layer)</br>
https://en.wikipedia.org/wiki/Wine_(software)</br>
https://en.wikipedia.org/wiki/Lutris</br>
