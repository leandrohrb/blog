---
title: "3D Graphics APIs"
date: 2019-11-08
draft: false
---

## Introduction

I'm really interested in **Vulkan/OpenGL, Mesa, DRM** and how the whole
**GNU/Linux Graphics Stack** works. I had lots of doubts about it, so I did a
research about the **history** of the 3D APIs. I want to share what I've
learned, hope you enjoy it. If you find something wrong or can help me to
enhance this blog, please contact me, I'll be thankful if you do so :)

### Brief history of computer graphics 3D APIs

When we hear about graphics programming we tend to think about games and
graphical interfaces. They are good examples and probably helped a lot in the
development of this area, but there are many other important applications that
can make use of it: art, design and simulations, for instance.

Not long ago, the computers used to work **without any GUI** (icons, images,
symbols, and not only text). The first one arrived in 1973, in a machine named
Xerox Auto. At that time, the development of the graphics stack in many systems
was in full swing, but the demand was only for **2D applications**.

<center>
<img src="/posts/3d-apis/xerox-alto.jpg"
     alt="Xerox Alto and its GUI"
     style="width:300px; margin:20px 0px 15px 0px" />
<p> Xerox Alto and its GUI. </p>
</center>

### OpenGL and Direct3D

This reality started to change in the 90's, when the home computer power was
big enough that people started to work on **3D applications in mass scale**.
This revolution was followed by the born of the first 3D capable GPUs and also
the frameworks **OpenGL and Direct3D**, which allowed 3D rendering with
hardware acceleration. Nothing was consolidated, so there was different ideas
coming from many game developers and this **lack of orientation** slowed down
the OpenGL development and created an uncertainty feeling about the future of
the technology. Direct3D was doing great, adding new features and making the
life of a game developer simpler. The game industry was rising strong, and they
couldn't count with something that could die in a brief future. We can't ignore
the marketing power of Microsoft as well.

<center>
<img src="/posts/3d-apis/opengl-directx.png"
     alt="OpenGL and DirectX logos"
     style="width:450px; margin:20px 0px 15px 0px" />
<p> The competitors: OpenGL and DirectX </p>
</center>

**OpenGL always had the advantaged to make easier to write multi-platform
applications**, but as time passed by creating games for Windows started to get
easier using Direct3D. Besides that, game developers started to get specialized
in Direct3D. This **created a loop**, in which the GPU vendors
focused to give a better support for D3D and graphics programmers specialized
even more in the technology. Also, with a much smaller community around the
OpenGL drivers it was hard to find the bugs for it.

### Microsoft and the Windows Vista episode

After the Vista release, Microsoft was trying to force users to migrate from
XP, so they've enabled support for D3D10 **only for Vista** (which was bad),
while XP users could only use D3D9 (even if they had hardware support for
D3D10). Fortunately, OpenGL was the way to go in the GNU/Linux graphics stack.
**With time and a strong community around it, OpenGL aged really well**. With
the Vista episode people started to realize how bad can be the monopoly of a
whole industry and the importance of OpenGL for the **future of graphics
programming**.  At that time, OpenGL had already created new features and was
faster than D3D9.  Nowadays, D3D11 is really optimized as well, but OpenGL's
portability still make it a strong competitor against Direct3D.

### Brand-new 3D APIs: Vulkan and Direct3D 12

In 2015, Khronos Group announced the **Vulkan API**, a low-level, low-overhead
and cross-platform 3D API. It offers **high performance** and is **less CPU
hungry**. Besides that, it's able to distribute balance between multiple CPU
cores. This is really important for mobile applications, since it means less
power consumption. Also, OpenGL survived many technology revolutions, and it
makes sense to think that Mesa3D's code may be a little bloated. Notice that
this does not means that OpenGL is bad and will be deprecated soon, since it's
good enough for the majority of the use cases. Vulkan is harder to learn and
the developer have to be explicit in everything he's doing (this is exactly
what makes it so powerful), what consumes more development time.

MS also created a low-level API named D3D12. It's supported both by Win 7 / 10
and the GPU of Xbox One. The familiarity of many companies with D3D and the
number of Win/Xbox users are one of the reasons why many companies are still
developing for D3D12 instead of Vulkan.

<center>
<img src="/posts/3d-apis/vulkan-directx12.jpg"
     alt="Vulkan and DirectX12 logos"
     style="width:500px; margin:20px 0px 15px 0px" />
<p> DirectX 12 and Vulkan </p>
</center>

### Wine and Proton

We may think that because a program is created only for Windows it won't run in
a GNU/Linux distribution, but that's not true. **Wine** is a compatibility
layer that allow applications developed for Windows to run on GNU/Linux and
other Unix-like systems (like macOS), but it's not easy to configure and use.
So there are other applications that were built using it: **Winetricks,
PlayOnLinux, Lutris, etc**. But one software that deservers attention is
**Proton**, a variation of Wine created by Valve and CodeWeavers in 2018.
This is a huge effort to make Steam able to offer a plug-and-play experience
for other systems, and not only Windows. The main goal of Valve is probably
to make SteamOS (Debian based) and the Steam Machine viable.

With this release, the majority of Windows games are **running pretty well** on
Linux. Sometimes the users have to do some minor tweaking to make it work, but
nothing really hard. This is really good, since one of the main reasons why
people don't migrate from Windows or are doing dual-boot is because of gaming.
Most of the games that are not working properly are those that uses anti-cheats
that **spy our processes** to find something suspicious. Honestly, I think we are
better without them...

Getting back to talk about the 3D APIs, Proton added a compatibility layer
between Direct3D and OpenGL/Vulkan in order to make many Windows games to work
on Linux. Those are called **translation layers**:

- **D9VK**: Direct3D 9 -> Vulkan
- **DXVK**: Direct3D 10 / 11 -> Vulkan (also works for OpenGL)
- **VKD3D**: Direct3D 12 -> Vulkan

<center>
<img src="/posts/3d-apis/protondb.png"
     alt="Vulkan and DirectX12 logos"
     style="width:500px; margin:20px 0px 15px 0px" />
<p> ProtonDB: a database where people add reports and small tweaks to make games work with Proton. </p>
</center>

### And that's all for today

I hope I've managed to clarify some stuff for you that is beginning to learn
graphics programming :) In the next blog-post I'll try to explain a little bit
about the GNU/Linux graphics stack.

### Further Reading
https://en.wikipedia.org/wiki/Computer_graphics</br>
https://en.wikipedia.org/wiki/3D_computer_graphics</br>
https://en.wikipedia.org/wiki/History_of_the_graphical_user_interface</br>
https://en.wikipedia.org/wiki/OpenGL</br>
http://blog.wolfire.com/2010/01/Why-you-should-use-OpenGL-and-not-DirectX</br>
https://pt.wikipedia.org/wiki/Vulkan</br>
https://www.imgtec.com/blog/stuck-on-opengl-es-time-to-move-on-why-vulkan-is-the-future-of-graphics/</br>
https://www.reddit.com/r/opengl/comments/3pdtk1/what_are_the_strength_and_weaknesses_of_opengl/</br>
https://www.reddit.com/r/linux/comments/81114g/why_do_game_companies_still_use_directx_instead/</br>
https://en.wikipedia.org/wiki/Proton_(compatibility_layer)</br>
https://en.wikipedia.org/wiki/Wine_(software)</br>
https://en.wikipedia.org/wiki/Lutris</br>
