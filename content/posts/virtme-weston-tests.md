---
title: "How to run Weston tests using virtme"
date: 2020-05-27
draft: true
---

## Introduction

Hello! In today's post we're going to show how to create a setup to run
**Weston** tests using **virtme**. So first let's explain what are Weston and
virtme.

### What is Weston?

**Weston** is the reference implementation of a Wayland compositor. It is
a minimal and fast compositor, written in C. Wayland itself is a simpler and
easier to maintain replacement for X. If you don't know much about X or
why is it being replace by Wayland, you can check the following links:

TODO: insert the links

### What is virtme?

**virtme** is a wraper around **qemu**. 

TODO: explain what is qemu and what is virtme.

## We need a kernel image

In order to run **virtme** (the same for **qemu**) we need a kernel image. So
we're going to clone Linus Torvalds's git tree and change our working directory
to it:

```
$ git clone https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
$ cd linux
```

Now we want to compile the kernel. But before running `make`, let's setup a few
things. First we want to edit the `.config` file to create a kernel image
suitable for our virtual machine. To enable virtualization stuff, run the
following. It will edit the `.config` for you:

```
$ make x86_64_defconfig
$ make kvmconfig
```

I like to have 2 cards in my setup: **VKMS** and **qemu's graphics card**.
That's because **VKMS** is what probably is going to run in CI's, but we can
not actually display images using it. So let's say we have a test where we have
to display a red square in the screen using Weston. I want to make sure that the
red square is working, so I just run it using qemu's graphics card.

In order to enable VKMS and qemu's graphics card, add the following to
`.config` (if not already set):

```
CONFIG_DRM_VKMS=y
CONFIG_DRM_BOCHS=y
```

We also want to install `ccache` (compiler cache). It will be useful to speed
up build times for the kernel. If this is the first time you compile it, it
will be slow anyway. But if you compile it again, it will be way faster. The
command to install it depends on your distribution, so I'm ommiting this. Now
let's compile the kernel using 4 threads and using **ccache**:

```
$ make CC="ccache gcc" -j4
```

Now we have a kernel image in `linux/arch/x86/boot/bzImage`.

## Installing virtme

In order to install virtme, let's first download its source code and move the
working directory to it:

```
$ git clone https://github.com/amluto/virtme
$ cd virtme
```

Now, simply run:

```
$ sudo python setup.py
```

Go back to the kernel folder and run the following command. You'll have to run
this every time you edit `.config` and compile the kernel again.

```
$ virtme-prep-kdir-mods
```

## Now let's finally run virtme

```
virtme-run --kdir linux --rw --graphics --qemu-opts -m 4G -smp 2
```

- `--kdir` points to the folder where we have the kernel image.
- `--rw` makes virtme run with read and write capability.
- `--graphics` is necessary if we want to use qemu's graphics card.
- `--qemu-opts` let us choose any option available in qemu. Here we set `m 4G`
  to let it use 4GB of RAM and `-smp 2` to run it in 2 cores.

If everything is working good, you can run the following command and see `card0`
and `card1`:

```
$ ls /dev/dri
```

You can also run the following command to retrieve information about the
devices. It is useful to find out in which node is each device (VKMS and qemu's
graphics card):

```
$ udevadm info /dev/dri/card0
$ udevadm info /dev/dri/card1
```

## How to run Weston tests

Now we are inside the virtual machine. First of all, let's create the runtime
directory and change its permissions. Then we config some environment variables
that are necessary in order to run the tests. Change the card to the one you
want to use to run DRM-backend tests:

```
$ mkdir -p /tmp/tests
$ chmod 0700 /tmp/tests
$ export XDG_RUNTIME_DIR=/tmp/tests
$ export WESTON_TEST_SUITE_DRM_DEVICE=card0
```

Now we are ready to run Weston tests. Change the working directory to Weston's
directory. Finally, run the tests.

```
$ cd weston/build
$ meson build
$ cd build
$ ninja test
```

You can see the detailed log in `weston/build/meson-logs/testlog.txt`. Also, if
a test fail Weston will show its log in `stdout`. In case we want to change the
card where DRM-backend tests run, we can simply run
`export WESTON_TEST_SUITE_DRM_DEVICE=card0` and then `ninja test` again.
